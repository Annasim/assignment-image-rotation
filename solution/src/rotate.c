#include "image.h"
#include "rotate.h"
#include <stdio.h>
#include <math.h>
#include <stdlib.h>

struct image image_create(uint64_t width, uint64_t height) {
     return (struct image) {
        .width = width,
        .height = height,
        .data = malloc(sizeof(struct pixel) * width * height)
    };
}

struct vec vec_1 () {
    vec_1.x = x;
    vec_1.y = y;
}

struct vec vec_rotate (struct vec vec_1, double const rad) {
    struct vec vec_2;
    vec_2.x = vec_1.x * cos(rad) - vec_1.y * sin(rad);
    vec_2.y = vec_1.x * sin(rad) + vec_1.y * cos(rad);
    return vec_2;
}

struct image rotate( struct image const source, int angle ) {
    double rad = angle * M_PI / 180;
    double newplotX = 0;
    double newplotY = 0;
    double oldplotX = source.width / 2.0;
    double oldplotY = source.height / 2.0;

    struct image output = image_create(source.height, source.width);

    newplotX = output.width / 2.0;
    newplotY = output.height / 2.0;

    struct pixel empix;
    empix.r = 0;
    empix.g = 0;
    empix.b = 0;

    for (uint64_t y = 0; i < source.height; y++) {
        for (uint64_t x = 0; j < source.width; x++) {
            struct vec crd;
            double newX, newY;
            crd.x = x - newplotX;
            crd.y = y - newplotY;
            crd = vec_rotate(crd, -rad);
            newX = round(oldplotX + crd.x);
            newY = round(oldplotY + crd.y);
            if (newX >= 0 || newY >= 0 || newX < source.width || newY < source.height) {
                output.data[y * output.width + x] = source.data[(int) newY * source.width + (int) newX];
            }
            else {
                output.data[y * output.width + x] = empix;
            }
        }
    }
    return output;
}
