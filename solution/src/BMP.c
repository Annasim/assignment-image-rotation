#define BI_SIZE 40
#define BI_PLANES 1
#define BI_BIT_COUNT 24
#include "BMP.h"
#include "image.h"
#include <stdio.h>
#include <stdlib.h>

#pragma pack(push, 1)
struct bmp_header
{
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};
#pragma pack(pop)

#define BFTYPE 0x4D42

//size_t fread( void * ptrvoid, size_t size, size_t count, FILE * filestream );
//int fseek( FILE * filestream, long int offset, int origin );
enum read_status from_bmp( FILE* in, struct image* img )
{
    if (!in)
        return READ_NULL;
    else
    {
    struct bmp_header* header = malloc(sizeof(struct bmp_header));
    if (fread(header, sizeof(struct bmp_header), 1, in) != 1)
        return READ_INVALID_HEADER;

    img->data = malloc(sizeof(struct pixel)*header->biWidth*header->biHeight);
    img->width = header->biWidth;
    img->height = header->biHeight;

    for(uint32_t i=0; i< header->biHeight; i++) {
       if (fread(&(img->data[i*img->width]), sizeof(struct pixel), header->biWidth, in) > header->biWidth) {
           free(img->data);
           img->data = NULL;
            return READ_INVALID_BITS;
        }
       if (fseek( in, header->biWidth%4, SEEK_CUR) != 0)
            return SEEK_ERROR;
    }
    free(header);
    return READ_OK;
    }
}
static uint32_t get_padding(uint32_t width)
{
    return (4 - (width * sizeof (struct pixel)) % 4) % 4;
}

static struct bmp_header create_header(const struct image* img) {
    struct bmp_header header = {0};
    header.bfType = BFTYPE;
    header.bfileSize = (sizeof(struct bmp_header)
                    + img->height* img->width * sizeof(struct pixel)
                    + img->height*get_padding(img->width));
    header.bfReserved = 0, //Зарезервирован и должен быть нулём
    header.bOffBits = sizeof(struct bmp_header);
    header.biSize = BI_SIZE;
    header.biWidth = img->width;
    header.biHeight = img->height;
    header.biPlanes = BI_PLANES;
    header.biBitCount = BI_BIT_COUNT;
    header.biCompression = 0;
    header.biSizeImage = header.bfileSize - 54;
    header.biXPelsPerMeter = 0;
    header.biYPelsPerMeter = 0;
    header.biClrUsed = 0;
    header.biClrImportant = 0;
    return header;
}

//size_t fwrite ( const void * ptr, size_t size, size_t count, FILE * stream );
enum write_status to_bmp( FILE* out, struct image const* img ) {
    struct bmp_header header = create_header(img);
    size_t const padding_value = 0;

    if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1)
        return WRITE_HEADER_ERROR;

    for(uint32_t h=0; h< img->height; h++) {
        if (fwrite(&(img->data[h*img->width]),sizeof(struct pixel), img->width, out) != img->width)
            return WRITE_PIXEL_ERROR;

        fwrite(&padding_value, 1, get_padding(img->width), out);
        }
    return WRITE_OK;
}
