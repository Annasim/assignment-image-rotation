#include "BMP.h"
#include "image.h"
#include "image_manip.h"
#include "rotate.h"
#include "util.h"

#include <stdio.h>
#include <stdlib.h>

int main( int argc, char** argv ) {
    if (argc != 3)
    {
        printf("Неверное количество аргументов\n");
        return 0;
    }

    struct image img = {0};
    
    struct image rotated_img = {0};
    
    FILE *file = fopen(argv[1], "rb");
    
    FILE *new_file = fopen(argv[2], "wb");

    switch (from_bmp(file, &img))
    {
    case READ_NULL:
        printf("Ошибка прочтения\n");
        return 1;
        break;
    case READ_INVALID_SIGNATURE:
        printf("Ошибка в чтении сигнатуры\n");
        return 1;
        break;
    case READ_INVALID_BITS:
        printf("Ошибка в чтении разрядов\n");
        return 1;
        break;
    case READ_INVALID_HEADER:
        printf("Ошибка в чтении заголовка\n");
        return 1;
        break;
    case SEEK_ERROR:
        printf("Ошибка поиска\n");
        return 1;
        break;
    case READ_OK:
        printf("Картинка прочиталась\n");
        break;
    }

    rotated_img = rotate(img);

    switch (to_bmp(new_file, &rotated_img))
    {
    case WRITE_ERROR:
        printf("Ошибка\n");
        return 1;
        break;
    case WRITE_HEADER_ERROR:
        printf("Ошибка записи заголовка\n");
        return 1;
        break;
    case WRITE_PIXEL_ERROR:
        printf("Ошибка записи пикселя\n");
        return 1;
        break;
    case WRITE_PADDING_ERROR:
        printf("Ошибка заполнения\n");
        return 1;
        break;
    case WRITE_OK:
        printf("Картинка записана\n");
        break;
    }

    int i,j,y, x;
    unsigned char byte[54];

    for(i = 0; i < 54; i++)            
    {                  
      byte[i] = getc(file);                
    }

    fwrite(byte,sizeof(unsigned char),54,new_file);      
 
    int height = *(int*)&byte[18];
    int width = *(int*)&byte[22];
    int bitDepth = *(int*)&byte[28];

    int size = height*width;          
    unsigned char buffer[size][3];          
    unsigned char out[size][3];          

    for(i = 0; i < size; i++)            
    {
        buffer[i][2]=getc(file);          
        buffer[i][1]=getc(file);          
        buffer[i][0]=getc(file);          
    }

    float v=1.0 / 9.0;            
    float kernel[3][3]={{v,v,v},
                        {v,v,v},
                        {v,v,v}};

    for(x = 1; x < height-1; x++)
    {          
        for(y = 1; y < width-1; y++)
        {
        float sum0= 0.0;
        float sum1= 0.0;
        float sum2= 0.0;
        for(i = -1; i <= 1; ++i)
        {
            for(j = -1; j <= 1; ++j)
            {  
            sum0=sum0+(float)kernel[i+1][j+1]*buffer[(x+i)*width+(y+j)][0];
            sum1=sum1+(float)kernel[i+1][j+1]*buffer[(x+i)*width+(y+j)][1];
            sum2=sum2+(float)kernel[i+1][j+1]*buffer[(x+i)*width+(y+j)][2];
            }
        }
        out[(x)*width+(y)][0]=sum0;
        out[(x)*width+(y)][1]=sum1;
        out[(x)*width+(y)][2]=sum2;
        }
    }

    for(i = 0; i < size; i++)            
    {
        putc(out[i][2],new_file);
        putc(out[i][1],new_file);
        putc(out[i][0],new_file);
    }
    
    rotated_img = DilateAndErodeFilter(img);

    fclose(file);
    fclose(new_file);
    free(img.data);
    free(rotated_img.data);
    return EXIT_SUCCESS;
}
