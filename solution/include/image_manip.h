#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_MANIP_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_MANIP_H

public static Bitmap DilateAndErodeFilter(
                           this Bitmap sourceBitmap,  
                           int matrixSize, 
                           MorphologyType morphType, 
                           bool applyBlue = true, 
                           bool applyGreen = true, 
                           bool applyRed = true );

#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGE_MANIP_H
